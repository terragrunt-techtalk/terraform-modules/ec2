output "instance_ids" {
  description = "IDs das instâncias criadas"
  value       = "${aws_instance.main.*.id}"
}

output "security_group_id" {
  description = "ID do security group criado para as instâncias"
  value       = "${aws_security_group.main.id}"
}
