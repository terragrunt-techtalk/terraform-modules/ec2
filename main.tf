data "aws_subnet" "selected" {
  id = "${var.subnet_ids[0]}"
}

resource "aws_instance" "main" {
  count = "${var.instance_count}"

  ami                         = "${data.aws_ami.amazon2.id}"
  instance_type               = "${var.instance_type}"
  user_data                   = "${var.user_data}"
  subnet_id                   = "${element(var.subnet_ids,count.index)}"
  associate_public_ip_address = "${var.associate_public_ip_address}"
  vpc_security_group_ids      = ["${aws_security_group.main.id}"]
  key_name                    = "${var.instance_key_name}"

  tags = "${
    merge(
      map(
        "Name","${var.name}"
        ),
      var.tags
    )
  }"
}

resource "aws_security_group" "main" {
  name        = "${var.name}-ec2-sg"
  description = "Security Group das instancias EC2 ${var.name}"
  vpc_id      = "${data.aws_subnet.selected.vpc_id}"
}

resource "aws_security_group_rule" "allow_egress" {
  type        = "egress"
  from_port   = 0
  to_port     = 65535
  protocol    = "-1"
  cidr_blocks = ["0.0.0.0/0"]

  security_group_id = "${aws_security_group.main.id}"
}

data "aws_ami" "amazon2" {
  most_recent = true

  filter {
    name   = "name"
    values = ["amzn2-ami-hvm-2.0.*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  filter {
    name   = "root-device-type"
    values = ["ebs"]
  }

  filter {
    name   = "architecture"
    values = ["x86_64"]
  }

  owners = ["137112412989"]
}
