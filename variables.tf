#-----------------------
# VARIAVEIS OBRIGATORIAS
#-----------------------
variable "name" {
  description = "Nome da instância EC2 e demais objetos criados para esse módulo"
}

variable "subnet_ids" {
  description = "Lista de IDs das subnets para serem utilizadas com as instâncias"
  type        = "list"
}

#--------------------
# VARIAVEIS OPCIONAIS
#--------------------

variable "instance_count" {
  description = "Número de instâncias EC2 a serem criadas"
  default     = 1
}

variable "instance_type" {
  description = "Tipo de instância EC2"
  default     = "t2.micro"
}

variable "associate_public_ip_address" {
  description = "Associar um endereço IP público para a instância"
  default     = "false"
}

variable "instance_key_name" {
  default = ""
}

variable "user_data" {
  description = "Script do User Data para ser executado na inicialização da instância"
  default     = ""
}

variable "tags" {
  description = "Tags para identificação dos recursos criados pelo módulo"
  default     = {}
}
